# README #


### What is this repository for? ###

* Here we'll work in a screening questionnaire for HC to ask to the candidates. 
* Version 1.0

### What is the screening used for? ###

From the technical standpoint, a screening is useful to: 

* Ensure the candidate has a basic knowledge of the tecnologies we use.
* The candidate has troubleshooting skills.
* The candidate understands basic concepts.
* English language skills. 

### Contributions ###

* This repo is open, you are more than welcome to contribute.
* Create a branch, clone the repo , add your contribution and then create a Pull Request.
* The Pull Request should count with at least 2 approvals (1 must be from a TL o Lead).

### I have dounts, Who do I talk to? ###

* [Alejandro Areco](aareco@edrans.com)
* [Maximiliano Esperanza](mesperanza@edrans.com) 
* [Cesar Allende](callende@edrans.com)
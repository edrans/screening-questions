# SCREENING QUESTIONS:

## What is GIT  and how would you use it?

Git is a version control system for tracking changes in computer files and coordinating work on those files among multiple people.

Explanation: this means that this software keeps tracking of the changes that you make in a particular project and you can also work with the projects’ files locally (on your computer) while different people is also working on the same files and contributing to the same project. The repository admin has to decide whether a change (pull request) can be merged to the project or not.



## What is a web server? Name any example.

A Web server is a program that uses HTTP (Hypertext Transfer Protocol) to serve the files that form Web pages to users, in response to their requests, which are forwarded by their computers' HTTP clients. Dedicated computers and appliances may be referred to as Web servers as well.

Explanation: it’s a software tool that allows users to reach and view web pages from their own web browsers. Web servers are usually installed on servers so they’re able to to receive and process requests from multiple users in parallel. The most well known web servers are Nginx, Apache, IIS (Microsoft).


## What are the different parts of an URL?

Explanation:

1. Protocol: http, https, ftp
2. Domain: www.google.com   (Subdomain: “www”,  Domain Name: “google.com”)
3. Port: 80, 8080, 443, 21
4. Path: /images/photo.jpg

Example: https://www.google.com:443/images/photo.jpg



## What is DNS?

The Domain Name System (DNS) is the phonebook of the Internet. Humans access information online through domain names, like nytimes.com or espn.com. Web browsers interact through Internet Protocol (IP) addresses. DNS translates domain names to IP addresses so browsers can load Internet resources.

Explanation: the DNS is a human readable name for an IP. Example: your own IP will be your D.N.I. but you are known by your Name, this name is your DNS address.





## What is the difference between a Relational and Non-Relational database?


Explanation: the main difference resides in the way that the information is organized. 
In a relational DataBase the information is stored in tables and those tables are related to each other by using Keys.
In a non relational DB there is no need to use tables so the data can be stored in a single file.

How will you obtain a list of all the databases in relational DB like MySQL?

Show databases;



![Databases](images/show_databases)



## What is Docker?

A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings. Containers are isolated from each other and bundle their own tools, libraries and configuration files; they can communicate with each other through well-defined channels. All containers are run by a single operating system kernel and are thus more lightweight than virtual machines.



Explanation: While a virtual machine initiates a whole operative system (O.S.) to run, let’s say for example, an application, a Docker container can just run a specific application just using the necessary resources.
Example: you could think in a VM as a house where the family that lives on that house is the O.S.. In the other hand, you could think in the Container machine as a building where every Application is an apartment. 



## What’s an orchestration software? What is used for?

Orchestration software controls the automation tasks that comprise company workflows, specifically for IT tasks. It’s used to reduce the time and effort for deploying multiple instances of a single application.

Explanation: the Provisioner Machine will act as an Orchestra Director and the servers will act as the Orchestra per se. This way, the director will send the changes that will be applied to the Orchestra (and therefore, the musicians (servers) that are part of it).



What are the benefits of using cloud computing instead of using an on premise architecture?
Name any cloud service that you know. (Take note of the services mentioned if the candidate names any)

 Cloud-based architecture is hosted on the vendor’s datacenters and accessed through a Web browser. It is cheaper, stable and easy to use.
 On-premise architecture is installed locally, on a company’s own computers and servers. 





## AWS Questions (Ssr+ / Sr candidates)

A) - A customer relationship management (CRM)application runs on Amazon EC2 instances in multiple Availability Zones behind an Application Load Balancer. If one of these instances fails, what occurs?

1. The load balancer will stop sending requests to the failed instance.
2. The load balancer will terminate the failed instance.
3. The load balancer will automatically replace the failed instance.
4. The load balancer will return 504 Gateway Timeout errors until the instance is replaced

1 –An Application Load Balancer (ALB) sends requests to healthy instances only. An ALB performs periodic health checks on targets in a target group. An instance that fails health checks for a configurable number of consecutive times is considered unhealthy. The load balancer will no longer send requests to the instance until it passes another health check. 

B) - A solutions architect wants to design a solution to save costs forAmazonEC2 instances that do not need to run during a 2-week company shutdown. The applications running on the instances store data in instance memory(RAM)that must be present when the instances resume operation.Which approach should the solutions architect recommend to shut down and resume the instances?

1. Modify the application to store the data on instance store volumes. Reattach the volumes while restarting them. 
2. Snapshot the instances before stopping them. Restore the snapshot after restarting the instances. 
3. Run the applications on instances enabled for hibernation. Hibernate the instances before the shutdown. 
4. Note the Availability Zone for each instance before stopping it. Restart the instances in the same Availability Zones after the shutdown.

3 –Hibernatingan instance saves the contents of RAM to the Amazon EBS root volume. When the instance restarts, the RAM contents are reloaded

C) - A company plans to run a monitoring application on an Amazon EC2 instance in a VPC. Connections are made to the instance using its private IPv4 address. Solutions architect needs to design a solution that will allow traffic to be quickly directed to a standby instance if the application fails and becomes unreachable.Which approach will meet these requirements?

1. Deploy an Application Load Balancer configured with a listener for the private IP address and register the primary instance with the load balancer. Upon failure, de-register the instance and register the secondary instance. 
2. Configure a custom DHCP option set. Configure DHCP to assign the same private IP address to the secondary instance when the primary instance fails.
3. Attach a secondary elastic network interface (ENI)to the instance configured with the private IP address. Move the ENI to the standby instance if the primary instance becomes unreachable. 
4. Associate an Elastic IP address with the network interface of the primary instance. Disassociate the Elastic IPfrom the primary instance upon failure and associate it with a secondary instance.

3 –A secondary ENIcan be added to an instance. While primary ENIs cannot be detached from an instance, secondary ENIs can be detached and attached to a different instance.

Source: https://d1.awsstatic.com/training-and-certification/docs-sa-assoc/AWS-Certified-Solutions-Architect-Associate_Sample-Questions.pdf
